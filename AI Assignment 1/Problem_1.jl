### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ a8b54880-b423-11eb-3906-0f89756c0358
using Pkg

# ╔═╡ 12737606-781b-4d2d-8517-80a567190ac6
Pkg.activate("Project.toml")

# ╔═╡ 523725c7-ae21-4669-8650-c062ab560af6
using PlutoUI

# ╔═╡ 185cae74-ad9f-4837-916c-1c08527822e4
using DataStructures

# ╔═╡ e238911f-5f81-42af-bb7a-70033fbd2d3b
mutable struct Office
	Name::String
	ItemCount::Int64
end

# ╔═╡ 651c726d-1d1b-4117-bf47-90654742d564
W = Office("West_Office", 1)

# ╔═╡ e8bb4b96-5939-4e8b-ac56-252e428b2ff0
C1 = Office("Center-Left_Office", 3)

# ╔═╡ 583e17f3-2186-40a9-9817-295ce61c1542
C2 = Office("Center-Right_Office", 2)

# ╔═╡ 893219a4-61fe-4c1b-94d2-0cd1c0956456
E = Office("East_Office", 1)

# ╔═╡ 34222a07-23e1-4c5f-8e28-6626ef04ce54
mutable struct Action 
	Name::String
	pickup::Int64
	Cost::Int64
end

# ╔═╡ 1c28531f-8bb0-4cfb-a311-f69a8bb69b7c
co = Action("Collect", 1, 5)

# ╔═╡ e62c87c6-d4fe-4c39-9835-2f75a366e1a8
me = Action("Move East", 0, 3)

# ╔═╡ 5a7d821f-44ca-45fd-9d2d-b59c9cf391e7
mw = Action("Move West", 0, 3)

# ╔═╡ 926e4d9c-52ed-4ad5-b146-ac10d4801574
re = Action("Remain", 0, 1)

# ╔═╡ 60195f91-53f3-42df-bc5a-0897fe64863c
struct VerbalPosition
	Verbal::Office
end

# ╔═╡ 680b199b-4152-4380-8309-5efde9eec381
Start = VerbalPosition(C1)

# ╔═╡ 3027e79f-81d2-47dc-b0d4-f0606197262f
Actions = Dict(Start => (re, C1))

# ╔═╡ da582033-7836-4d24-bfcd-7741d3b83462
push!(Actions, Start => (mw, W)) 

# ╔═╡ c85fdcea-5b94-44ce-b911-51792eaf38ba
push!(Actions, Start => (me, C2)) 

# ╔═╡ 1a62f928-3621-408f-8b86-5870b252fdea
push!(Actions, Start => (co, C1))

# ╔═╡ ec2359f2-3c14-4b3f-9615-9a925c95cb8f
function A_Star(Actions, Start)
	h = 7
	explored = []
	explore = Queue{Position}()
	enqueue!(explore, Start)
	while true 
		if empty(explore)
			return []
		else
			CurrentState = dequeue!(explore)
			push!(explored, CurrentState)
			for element in nextPosition
				if !(element[2] in explored)
					if element[2] in goal
						return CreateResult()
						
					else
						enqueue!(explore, element[2])
					end
				end
			end
		end
	end
end

# ╔═╡ 53e61658-e5b1-4184-8031-746466d236ee
function createResult(Actions, explored, path)
	result = []
	for i in 1:(length(explored)-1)
		var1 = Actions[explored[i]]
		for element in var1
			if i == (length(explored)-1)
				if element[2] == Path
				push!(result, element[1])
				end
			else
				if element[2] == explored[i+1]
				push!(result, element[1])
				end
			end
		end
	end
	return result
end

# ╔═╡ Cell order:
# ╠═a8b54880-b423-11eb-3906-0f89756c0358
# ╠═12737606-781b-4d2d-8517-80a567190ac6
# ╠═523725c7-ae21-4669-8650-c062ab560af6
# ╠═185cae74-ad9f-4837-916c-1c08527822e4
# ╠═e238911f-5f81-42af-bb7a-70033fbd2d3b
# ╠═651c726d-1d1b-4117-bf47-90654742d564
# ╠═e8bb4b96-5939-4e8b-ac56-252e428b2ff0
# ╠═583e17f3-2186-40a9-9817-295ce61c1542
# ╠═893219a4-61fe-4c1b-94d2-0cd1c0956456
# ╠═34222a07-23e1-4c5f-8e28-6626ef04ce54
# ╠═1c28531f-8bb0-4cfb-a311-f69a8bb69b7c
# ╠═e62c87c6-d4fe-4c39-9835-2f75a366e1a8
# ╠═5a7d821f-44ca-45fd-9d2d-b59c9cf391e7
# ╠═926e4d9c-52ed-4ad5-b146-ac10d4801574
# ╠═60195f91-53f3-42df-bc5a-0897fe64863c
# ╠═680b199b-4152-4380-8309-5efde9eec381
# ╠═3027e79f-81d2-47dc-b0d4-f0606197262f
# ╠═da582033-7836-4d24-bfcd-7741d3b83462
# ╠═c85fdcea-5b94-44ce-b911-51792eaf38ba
# ╠═1a62f928-3621-408f-8b86-5870b252fdea
# ╠═ec2359f2-3c14-4b3f-9615-9a925c95cb8f
# ╠═53e61658-e5b1-4184-8031-746466d236ee
