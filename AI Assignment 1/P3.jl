### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ ed1d6300-b40e-11eb-1c8c-95feb36181b8
 using Pkg


# ╔═╡ 1d4a61c2-2ed5-4144-be4b-05c1f57a7890
 Pkg.activate("Project.toml")


# ╔═╡ 0d051d05-7670-458b-ba93-78dc7a690d94
struct elementDomain 
	
	elements::Vector{Int64}
end 

# ╔═╡ 5079278a-69f4-48ab-9aaa-969c2d4ef480
mutable struct ConstraintSP 
	name::String
value::Union{Nothing,elementDomain }
forbidden_values::Vector{elementDomain}
domain_restriction_count::Int64
end


# ╔═╡ 3b3dc223-09d7-4510-b7ad-5397680ffd71
struct DomainCSP
vars::Vector{ConstraintSP} 
constraints::Vector{Tuple{ConstraintSP,ConstraintSP}}
end 
	


# ╔═╡ 32abd597-8099-4465-be2a-5d25744e56b1
 function backTrack( csp::DomainCSP, assignment) 
	
	
	for var in csp.vars 
		
	if var.domain_restriction_count == 4
      return []
          nextValue =rand(set(elements()), Set(val.forbidden_values ))
			 var.value = nexValue
	
	
	
	
	
	for constraint in csp.constraints[1] == var || (constraint[2]== var))
				
			else
if cur_constraint[1] == cur_var
push!(constraint[2].forbidden_values, nextValue)
cur_constraint[2].domain_restriction_count += 1

else
push!(constraint[1].forbidden_values,nextValue)
cconstraint[1].domain_restriction_count += 1
					
					end
end
end

push!(assignment), var.name => nextvalue)
end
end
return assignments
end



# ╔═╡ da6ad8be-1379-4235-a38c-01f8089864ef


# ╔═╡ Cell order:
# ╠═ed1d6300-b40e-11eb-1c8c-95feb36181b8
# ╠═1d4a61c2-2ed5-4144-be4b-05c1f57a7890
# ╠═5079278a-69f4-48ab-9aaa-969c2d4ef480
# ╠═0d051d05-7670-458b-ba93-78dc7a690d94
# ╠═3b3dc223-09d7-4510-b7ad-5397680ffd71
# ╠═32abd597-8099-4465-be2a-5d25744e56b1
# ╠═da6ad8be-1379-4235-a38c-01f8089864ef
