### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ fdb4c0e0-ae6a-11eb-0ab3-c11425247750
using Pkg

# ╔═╡ 89683b4b-ee81-4902-a213-b634fdbdc504
Pkg.activate("Project.toml")

# ╔═╡ 1943ef71-4090-4999-b718-875d45887ec9

using LeftChildRightSiblingTrees


# ╔═╡ 0a30136c-5a91-4e52-8024-c0905172357e
using AbstractTrees

# ╔═╡ f179f3e9-355a-4105-800f-3629990416fb
using PlutoUI

# ╔═╡ 51a15d71-a120-4643-8faa-4842e45f8fc0
md"# Problem 2 Assignment"

# ╔═╡ 45a69a2f-0a6e-4d45-b45f-b38483a87f27


# ╔═╡ feeb8de1-fc01-45e7-bcd0-e1f93a6802a5
println(LeftChildRightSiblingTrees.children)

# ╔═╡ 2c86a293-1220-4920-ac1f-2656c1bd061c
md"#  Alpha beta Pruning"

# ╔═╡ a72fc532-9bde-459b-9e65-1d9e9a14b08e
md"# Nodes"

# ╔═╡ a86c8b11-cb31-4c5c-b782-3eec700cebad
abstract type AdversarialNode end

# ╔═╡ 17ef367e-f51f-4394-b053-edba461ed889
struct TerminalNode <: AdversarialNode
	utility ::Int64
end

# ╔═╡ 142d1dd1-aae1-4b0f-a06f-a82d858e48e4
struct TerminalNodeArray
	nodes::Array{Any,1}
end

# ╔═╡ 0fb2ad1b-70bf-451b-a1e4-eafbe57c53c0
 struct PlayerAction 
	MaximizingPlayer::Vector{Bool}
end

# ╔═╡ d4314d43-2e49-4b61-8560-76866ad0b1aa
struct PlayerNode <: AdversarialNode
	action::PlayerAction
	nodes:: Vector{Union{TerminalNode,PlayerNode}}
	alphaValue::Int64
	BetaValue::Int64
	
end

# ╔═╡ b0f12dcc-0aa3-476d-a7e5-7d6c3c604bff
struct Game 
	initial:: PlayerNode
end

# ╔═╡ 53c7d576-3d83-44ac-9606-def223c69ddd
struct depth
	Depth::Int64
end

# ╔═╡ d6ced486-bf23-407b-81a5-1e0e1ffa339d
function minimax(node::PlayerNode,depth::Int64,alpha::Float64,beta::Float64, maximizingPlayer::PlayerAction)
	NODES=[PlayerNode]
	
	

	
if  isequal(depth,depth(0)) || isequal(node,TerminalNode)
		
return TerminalNode
	
	if PlayerAction([true])
			maxEva= -Inf
		for child in NODES
				eva=minimax(map(child-> child,node.nodes),depth.Depth,alpha.alphaValue,beta.BetaValue,false)
				maxEva= max(eva,maxEva)
	  			alpha.alphaValue=max(alpha.alphaValue,maxEva)
		
			
				if beta.Betavalue <= alpha.alphaValue
			
					break
			end
					return maxEva
				
			end
				else
					minEva=Inf
	         for child in NODES
				eva=minimax(map(child->child,node.nodes),depth.Depth,alpha.alphaValue,beta.BetaValue,true)
				minEva = min(minEva,eva)
				beta.BetaValue=min(beta.BetaValue,maxEva)
							
				
				if beta.Betavalue<=alpha.alphaValue
				
				break
						
					end
		    return minEva
		
	       
            end
	     end 
	   end
	end

# ╔═╡ d52bbac3-f7d5-4728-b02b-b94833b38b63


# ╔═╡ de7d4a6f-4567-4a6c-92dc-5e6feb521067


# ╔═╡ Cell order:
# ╠═51a15d71-a120-4643-8faa-4842e45f8fc0
# ╠═fdb4c0e0-ae6a-11eb-0ab3-c11425247750
# ╠═1943ef71-4090-4999-b718-875d45887ec9
# ╠═0a30136c-5a91-4e52-8024-c0905172357e
# ╠═f179f3e9-355a-4105-800f-3629990416fb
# ╠═89683b4b-ee81-4902-a213-b634fdbdc504
# ╠═45a69a2f-0a6e-4d45-b45f-b38483a87f27
# ╟─feeb8de1-fc01-45e7-bcd0-e1f93a6802a5
# ╠═2c86a293-1220-4920-ac1f-2656c1bd061c
# ╟─a72fc532-9bde-459b-9e65-1d9e9a14b08e
# ╠═a86c8b11-cb31-4c5c-b782-3eec700cebad
# ╠═17ef367e-f51f-4394-b053-edba461ed889
# ╠═142d1dd1-aae1-4b0f-a06f-a82d858e48e4
# ╠═0fb2ad1b-70bf-451b-a1e4-eafbe57c53c0
# ╠═d4314d43-2e49-4b61-8560-76866ad0b1aa
# ╠═b0f12dcc-0aa3-476d-a7e5-7d6c3c604bff
# ╠═53c7d576-3d83-44ac-9606-def223c69ddd
# ╠═d6ced486-bf23-407b-81a5-1e0e1ffa339d
# ╟─d52bbac3-f7d5-4728-b02b-b94833b38b63
# ╠═de7d4a6f-4567-4a6c-92dc-5e6feb521067
